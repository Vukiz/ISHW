import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.Math.abs;

public class Main {

    private static String folderName = "C:\\Programs\\JDK unzipped";
    private static ArrayList<String> words = new ArrayList<>() {{
        add("transparent");
        add("volatile");
    }};

    public static void main(String[] args)
    {
        //Rot13("The Quick Brown Fox Jumps Over The Lazy Dog");
        //System.out.println(HexToDecimal("FFF"));
        //System.out.println(gcd(-50,-10)+"\n"+gcdfloorMod(-50,-10)+"\n"+gcdRem(-50,-10));
        //ScannerToStream();
        //FindWordsInFiles(folderName, words);
        //FindCharacterInFile("words.txt");
        //ReadFileIntoList();
        //CreateInfiniteStream();
        AlternateStreams();
    }

    //task 9
    private static void AlternateStreams()
    {
        Stream<Integer> streamFirst = Stream.of(1, 2, 3, 4, 5);
        Stream<Integer> streamSecond = Stream.of(10, 9, 8, 7, 6, 5);

        Stream<Integer> stream = zip(streamFirst, streamSecond);

        stream.forEach(elem -> System.out.print(elem + " "));
    }
    public static <T> Stream zip(Stream<T> first, Stream<T> second)
    {
        List<T> listFirst = first.collect(Collectors.toList());
        List<T> listSecond = second.collect(Collectors.toList());

        ArrayList<T> mergedList = new ArrayList<>();

        for (int i = 0; i < listFirst.size() && i < listSecond.size(); i++)
        {
            mergedList.add(listFirst.get(i));
            mergedList.add(listSecond.get(i));
        }
        return mergedList.stream();
    }

    //task 8
    private static void CreateInfiniteStream()
    {
        long x0 = 5L;
        long a = 25214903917L;
        long c = 11L;
        long m = (long)Math.pow((double)2, (double)48);

        Stream<Long> generator = Stream.iterate(x0, x -> (a * x + c) % m).limit(20);

        generator.forEach(System.out::println);
    }

    //task 7
    private static void ReadFileIntoList()
    {
        final int assumedLineLength = 4;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter file name to read from");
        String fileName = scanner.next();
        System.out.println("Enter number of line to randomly show");
        int linesShowCount = scanner.nextInt();
        scanner.close();

        Path pathToFile = Paths.get(fileName);
        File file = pathToFile.toFile();
        List<String> fileList = new ArrayList<>((int)(file.length() / assumedLineLength));
        long lineCount;

        try(Stream<String> lines = Files.lines(pathToFile)) {
            lines.forEach(line -> fileList.add(line));
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        lineCount = fileList.size();

        Random random = new Random();
        for (int i = 0; i < linesShowCount; i++) {
            System.out.println((i+1) + ". " + fileList.get(random.nextInt(Math.toIntExact(lineCount) - 1)));
        }
    }

    //task 6
    private static void FindCharacterInFile(String fileToCheck)
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter character for search");
        char characterToCount = scanner.next().charAt(0);

        Path pathToFile = Paths.get(".\\"+fileToCheck);
        ArrayList<Integer> characterCount = new ArrayList();
        try(Stream<String> lines = Files.lines(pathToFile)) {
            lines.forEach((line)-> characterCount.add((int) line.chars().filter(ch -> ch == characterToCount).count())
            );
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        int sum= 0;
        for (Integer el:
             characterCount) {
            sum += el;
        }
        System.out.println(sum);
    }

    //task 5
    private static boolean checkStringForWords(String stringToCheck, List<String> wordsToCheck)
    {
        for (String word : wordsToCheck) {
            if(stringToCheck.toLowerCase().contains(word.toLowerCase()))
            {
                return true;
            }
        }
        return false;
    }
    private static void FindWordsInFiles(String FolderName, List<String> wordsToSearch)
    {
        Path pathToRoot = Paths.get(FolderName); // Unzipped src.zip

        try(Stream<Path> fileStream = Files.walk(pathToRoot).filter(Files::isRegularFile))
        {
            fileStream.forEach((file)->
            {
                try(Stream<String> lines = Files.lines(file, Charset.forName("UTF-8")))
                {
                    if (lines.anyMatch(l -> checkStringForWords(l, wordsToSearch)))
                    {
                        System.out.println(file);
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            });
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    //task 4
    public static void ScannerToStream()
    {
        //int
        ToIntStream("int.txt");
        //doubles
        ToDoubleStream("doubles.txt");
        //words
        ToWordsStream("words.txt");
        //lines
        ToLinesStream("lines.txt");
    }
    private static void ToLinesStream(String fileName)
    {
        try(FileInputStream stream = new FileInputStream(fileName))
        {
            Scanner sc = new Scanner(stream);
            ArrayList<String> linesList = new ArrayList<>();

            System.out.println("Reading lines from "+ fileName);

            while (sc.hasNext())
            {
                linesList.add(sc.nextLine());
            }
            Stream<String> wordsStream = linesList.stream();
            System.out.println("LinesStream: " + Arrays.toString(wordsStream.toArray(String[]::new)));
            wordsStream.close();
            sc.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    private static void ToWordsStream(String fileName)
    {
        try(FileInputStream stream = new FileInputStream(fileName))
        {
            Scanner sc = new Scanner(stream);
            ArrayList<String> wordsList = new ArrayList<>();

            System.out.println("Reading words from "+ fileName);

            while (sc.hasNext())
            {
                wordsList.add(sc.next());
            }
            Stream<String> wordsStream = wordsList.stream();
            System.out.println("LinesStream: " + Arrays.toString(wordsStream.toArray(String[]::new)));
            wordsStream.close();
            sc.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    private static void ToIntStream(String fileName)
    {
        try(FileInputStream stream = new FileInputStream(fileName))
        {
            Scanner sc = new Scanner(stream);
            ArrayList<Integer> IntList = new ArrayList<>();

            System.out.println("Reading Ints from "+ fileName);

            while (sc.hasNext())
            {
                if (sc.hasNextInt())
                {
                    IntList.add(sc.nextInt());
                }
                else
                {
                    sc.next();
                }
            }
            Stream<Integer> ints = IntList.stream();
            System.out.println("IntStream: " + Arrays.toString(ints.toArray(Integer[]::new)));
            ints.close();
            sc.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    private static void ToDoubleStream(String fileName)
    {
        try(FileInputStream stream = new FileInputStream(fileName))
        {
            Scanner sc = new Scanner(stream);
            ArrayList<Double> doubleArrayList = new ArrayList<>();
            System.out.println("Reading doubles from "+ fileName);
            while (sc.hasNext())
            {
                if (sc.hasNextDouble())
                {
                    doubleArrayList.add(sc.nextDouble());
                }
                else
                {
                    sc.next();
                }
            }
            Stream<Double> doubles = doubleArrayList.stream();
            System.out.println("DoubleStream: " + Arrays.toString(doubles.toArray(Double[]::new)));
            doubles.close();
            sc.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    //task 3
    public static long HexToDecimal(String hexString)
    {
        String[] digits = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
        List<String> digitsList = Arrays.asList(digits);
        hexString = hexString.toUpperCase();
        int result = 0;
        for (int i = 0; i < hexString.length(); i++)
        {
            char c = hexString.charAt(i);
            int d = digitsList.indexOf(String.valueOf(c));
            result = result * 16 + d;
        }
        return result;
    }

    //task 2
    public static void Rot13(String input)
    {
        System.out.println(input);
        String s = input;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if       (c >= 'a' && c <= 'm') c += 13;
            else if  (c >= 'A' && c <= 'M') c += 13;
            else if  (c >= 'n' && c <= 'z') c -= 13;
            else if  (c >= 'N' && c <= 'Z') c -= 13;
            System.out.print(c);
        }
        System.out.println();
    }

    //task 1
    public static int gcd(int p, int q)
    {
        if (q == 0)
        {
            return p;
        }
        return gcd(q, p % q);
    }

    public static int gcdfloorMod(int p, int q) {
        if (q == 0)
        {
            return p;
        }
        return gcdfloorMod(q, Math.floorMod(p,q));
    }
    public static int gcdRem(int p, int q) {
        if (q == 0)
        {
            return p;
        }
        return gcdfloorMod(q, p>q?p-q:q-p);
    }
}
